/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
const getSum = (node) => {
    if (!node.parent) return 0;
    let parentVal = node.parent.val;
    let left = node.left?.val ?? 0;
    let right = node.right?.val ?? 0;

    return (parentVal % 2 === 0) ? left + right : 0;
};

const sumGrandchildren = (node, parent) => {
    if (!node) return 0;

    node = {
        ...node,
        parent,
    };

    let sum = (node) ? getSum(node) : 0;
    return sum + sumGrandchildren(node.left, node) + sumGrandchildren(node.right, node);
};

/**
 * @param {TreeNode} root
 * @return {number}
 */
const sumEvenGrandparent = (root) => {
    return sumGrandchildren(root);
};

let data = {
    val: 6,
    left: {
        val: 7,
        left: {
            val: 2,
        },
        right: {
            val: 7,
        },
    },
    right: {
        val: 8,
        left: {
            val: 1,
        },
        right: {
            val: 3,
            right: {
                val: 5,
            },
        },
    },
};

console.log(sumEvenGrandparent(data));
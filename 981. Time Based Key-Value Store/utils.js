let testcases = require("./testcases");

let makeItRight = (arr) => {
    return JSON.stringify(arr);
};

let convertTestCase = (testcase) => {
    let { calls, params } = testcase;
    let test = new Set();
    test.add("let map = new TimeMap();")

    for (let i = 1; i < calls.length; i++) {
        test.add(`map["${calls[i]}"](...${makeItRight(params[i])});`);
    }

    return [...test].join("\n");
};

console.log(convertTestCase(testcases["0"]))
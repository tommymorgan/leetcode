let { performance } = require("perf_hooks");

class TimeMap {
	constructor() {}

	/**
	 * map = {
	 *    foo: {
	 *      timestamps
	 *      1:
	 *    }
	 * }
	 */

	map = {};

    #startTime = performance.now();

    get start() {
        return this.#startTime;
    }

    get end() {
        return performance.now();
    }

	/**
	 * @param {string} key
	 * @param {string} value
	 * @param {number} timestamp
	 * @return {void}
	 */
	set(key, value, timestamp) {
		this.map[key] = this.map[key] || {};
		this.map[key][timestamp] = value;
	}

	/**
	 * @param {string} key
	 * @param {number} timestamp
	 * @return {string}
	 */
	get(key, timestamp) {
		// return the most recent value or ""
		// (new Set(Object.keys(this.map))).has(key)
		if (this.map[key] === undefined) return "";

		let result;
		while (result === undefined && timestamp >= 0) {
			result = this.map[key] && this.map[key][+timestamp];
			timestamp -= 1;
		}

		return result === undefined ? "" : result;
	}
}

/**
 * Your TimeMap object will be instantiated and called as such:
 * var obj = new TimeMap()
 * obj.set(key,value,timestamp)
 * var param_2 = obj.get(key,timestamp)
 */

module.exports = TimeMap;

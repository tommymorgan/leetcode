let map = {};

let generateCode = () => {
    let chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    let code = "";

    for (let i = 0; i <= 6; i++) {
        let char = chars.charAt(Math.floor(Math.random() * (chars.length - 1)))
        code += char;
    }
    
    return code;
};

/**
 * Encodes a URL to a shortened URL.
 *
 * @param {string} longUrl
 * @return {string}
 */
let encode = (longUrl) => {
    let code = generateCode();
    
    while (map[code] !== undefined) {
        code = generateCode();
    }
    
    map[code] = longUrl;
    return code;
};

/**
 * Decodes a shortened URL to its original URL.
 *
 * @param {string} shortUrl
 * @return {string}
 */
let decode = (shortUrl) => {
    return map[shortUrl];
};

/**
 * Your functions will be called as such:
 * decode(encode(url));
 */
/**
 * Definition for a binary tree node.
 */
function TreeNode(val, left, right) {
  this.val = val === undefined ? 0 : val;
  this.left = left === undefined ? null : left;
  this.right = right === undefined ? null : right;
}

/**
 * edge cases
 * * node is null
 * * there is no kth smallest element
 */

let toArray = (values) =>
  (node) => {
    if (!node) return;
    values.push(node.val);
    toArray(values)(node.left);
    toArray(values)(node.right);
  };

/**
 * @param {TreeNode} root
 * @param {number} k
 * @return {number}
 */
let kthSmallest = (root, k) => {
  // flatten to array
  let values = [];
  toArray(values)(root);
  values.sort((a, b) => +a < +b ? -1 : 1);

  console.log(values);

  if (values.length >= k) return values[k - 1];

  return undefined;
};

let tests = [
  //   [
  //     new TreeNode(3, new TreeNode(2, new TreeNode(1)), new TreeNode(4, null, new TreeNode(5))),
  //     2,
  //   ],
  [
    new TreeNode(
      41,
      new TreeNode(
        37,
        new TreeNode(
          24,
          new TreeNode(
            1,
            new TreeNode(0),
            new TreeNode(
              2,
              null,
              new TreeNode(
                4,
                new TreeNode(3),
                new TreeNode(
                  9,
                  new TreeNode(
                    7,
                    new TreeNode(6, new TreeNode(5)),
                    new TreeNode(8),
                  ),
                  new TreeNode(
                    11,
                    new TreeNode(10),
                    new TreeNode(
                      16,
                      new TreeNode(
                        15,
                        new TreeNode(
                          12,
                          new TreeNode(13, null, new TreeNode(14)),
                        ),
                      ),
                      new TreeNode(
                        19,
                        new TreeNode(18, new TreeNode(17)),
                        new TreeNode(
                          20,
                          null,
                          new TreeNode(22, new TreeNode(21), new TreeNode(23)),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
          new TreeNode(
            35,
            new TreeNode(
              30,
              new TreeNode(
                29,
                new TreeNode(
                  26,
                  new TreeNode(25),
                  new TreeNode(27, null, new TreeNode(28)),
                ),
              ),
              new TreeNode(
                32,
                new TreeNode(31),
                new TreeNode(34, new TreeNode(33)),
              ),
            ),
            new TreeNode(36),
          ),
        ),
        new TreeNode(
          39,
          new TreeNode(38),
          new TreeNode(40, null, new TreeNode(43)),
        ),
      ),
      new TreeNode(
        44,
        new TreeNode(42),
        new TreeNode(
          48,
          new TreeNode(46, new TreeNode(45), new TreeNode(47)),
          new TreeNode(49),
        ),
      ),
    ),
    25,
  ],
];

tests.forEach((test) => console.log(kthSmallest(...test)));

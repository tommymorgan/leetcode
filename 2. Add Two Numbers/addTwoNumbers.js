let getNums = (item) => {
    let nums = [];
    do {
        nums.push(item.val);
    } while (item = item.next);

    return nums;
};

class ListNode {
    constructor(val, next) {
        this.val = val ?? 0;
        this.next = next ?? null;
    }
};

class LinkedList {
    constructor() {
        this.head;
        this.end;
    }

    insertAtEnd(item) {
        let node = new ListNode(item);

        if (this.head) this.end.next = node;
        else this.head = node;

        this.end = node;
    }

    toArray() {
        let nums = [];
        let node = this.head;
        do {
            nums.push(node.val);
        } while (node = node.next)

        return nums;
    }

    static fromLinkedList(list) {
        let l = new LinkedList();
        l = list;

        return l;
    }

    static fromArray(arr) {
        let list = new LinkedList();
        for (let x of arr) {
            list.insertAtEnd(x);
        }

        return list;
    }
};

/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} l1
 * @param {ListNode} l2
 * @return {ListNode}
 */
let addTwoNumbers = (l1, l2) => {
    let num2 = getNums(l2).reverse().join("");
    let num1 = getNums(l1).reverse().join("");
    let sum = BigInt(num1) + BigInt(num2);
    let nums = `${sum}`.split("").reverse().map((x) => +x);
    let list = LinkedList.fromArray(nums);

    return list;
};

// let test = {
//     l1: LinkedList.fromArray([1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1]),
//     l2: LinkedList.fromArray([5,6,4]),
//     expected: [6,6,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
// };

// console.log(addTwoNumbers(test.l1.head, test.l2.head).toArray());

const { describe, expect, test } = require("@jest/globals");
const isValid = require("./index");

describe("20. Valid Parentheses", () => {
    test("very simple valid case", () => {
        expect(isValid("{}")).toBe(true);
    });

    test("very simple invalid case", () => {
        expect(isValid("{)")).toBe(false);
    });

    test("simple valid case (without nesting)", () => {
        expect(isValid("{}()[]")).toBe(true);
    });

    test("simple valid case 2 (with nesting)", () => {
        expect(isValid("{()}[]")).toBe(true);
    });

    test("simple invalid case 1 (with nesting)", () => {
        expect(isValid("[{)]")).toBe(false);
    });

    test("simple invalid case 2 (with nesting)", () => {
        expect(isValid("{[])")).toBe(false);
    });

    test("matches must start with valid starting character", () => {
        expect(isValid("(){}}{")).toBe(false);
    });
});
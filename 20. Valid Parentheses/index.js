/**
 * @param {string} s
 * @return {boolean}
 */
let isValid = (s) => {
	let it = s.split("");
	let matches = {
		"(": ")",
		"{": "}",
		"[": "]",
	};

	// find first adjacent pair
	for (let i = 0; i < it.length; i++) {
		if (matches[it[i - 1]] === it[i]) {
            // remove the match and start over; wash, rinse, repeat
            it.splice(i - 1, 2);
            i = 0;
        }
	}

	// if we've removed all entries then everything matched up
    return it.length === 0;
};

module.exports = isValid;

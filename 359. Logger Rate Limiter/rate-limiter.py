class Logger:

    def __init__(self):
        self.log = {}
        

    def shouldPrintMessage(self, timestamp: int, message: str) -> bool:
        # if we've seen this message before
        if message in self.log:
            # and it was within the last ten seconds
            if timestamp - self.log[message] < 10:
                return False
        
        self.log[message] = timestamp
        return True

            
# Your Logger object will be instantiated and called as such:
# obj = Logger()
# param_1 = obj.shouldPrintMessage(timestamp,message)

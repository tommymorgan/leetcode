
var Logger = function() {
    this.log = {};
};

/** 
 * @param {number} timestamp 
 * @param {string} message
 * @return {boolean}
 */
Logger.prototype.shouldPrintMessage = function(timestamp, message) {
    // if we've seen this message before
    if (Object.keys(this.log).indexOf(message) >= 0) {
        // and it was within the last ten seconds
        if (timestamp - this.log[message] < 10) {
            return false;
        }
    }
    
    this.log[message] = timestamp;
    return true;
};

/** 
 * Your Logger object will be instantiated and called as such:
 * var obj = new Logger()
 * var param_1 = obj.shouldPrintMessage(timestamp,message)
 */

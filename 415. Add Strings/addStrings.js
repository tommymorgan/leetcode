let makeEqualLength = (s1, s2) => {
    let max = Math.max(s1.length, s2.length);
    let min = Math.min(s1.length, s2.length);
    let padding = ((new Array(max - min)).fill(0)).join("");

    if (s1.length < s2.length) s1 = `${padding}${s1}`;
    else s2 = `${padding}${s2}`;

    return [s1, s2];
};

/**
 * @param {string} s1
 * @param {string} s2
 * @return {string}
 */
let addStrings = (s1, s2) => {
    [s1, s2] = makeEqualLength(s1, s2);
    s1 = s1.split("").reverse();
    s2 = s2.split("").reverse();
    let result = new Array(s1.length);

    let n = 0;
    let carry = 0;
    for (let x = 0; x < result.length; x++) {
        n = (+s1[x] + +s2[x]) + carry;
        carry = (n >= 10) ? 1 : 0;
        n = n - carry * 10;
        result[x] = n;
    }

    if (carry === 1) result.push(1);

    return result.reverse().join("");
};

// let test = {
//     s1: "999",
//     s2: "18",
//     expected: "1017",
// };

// console.log(addStrings(test.s1, test.s2));
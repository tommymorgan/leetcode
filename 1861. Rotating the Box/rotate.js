/*
You are given an m x n matrix of characters box representing a side-view of a box. Each cell of the box is one of the following:

    A stone '#'
    A stationary obstacle '*'
    Empty '.'

The box is rotated 90 degrees clockwise, causing some of the stones to fall due to gravity. Each stone falls down until it lands on an obstacle, another stone, or the bottom of the box. Gravity does not affect the obstacles' positions, and the inertia from the box's rotation does not affect the stones' horizontal positions.

It is guaranteed that each stone in box rests on an obstacle, another stone, or the bottom of the box.

Return an n x m matrix representing the box after the rotation described above.
*/

let findNextHighestStone = (box, startingRow, col) => {
    for (let row = startingRow; row >= 0; row--) {
        if (box[row][col] === "*") return -1;
        if (box[row][col] === "#") return row;
    }

    return -1;
};

let gravity = (box) => {
	for (let row = box.length - 1; row >= 0; row--) {
		for (let col = 0; col < box[row].length; col++) {
			// if current space is empty
			if (box[row][col] === ".") {
				// and there's a row above
				if (row > 0) {
					// and there is a droppable stone above
                    let stoneRow = findNextHighestStone(box, row, col);
					if (stoneRow >= 0) {
						box[row][col] = "#";
						box[stoneRow][col] = ".";
					}
				}
			}
		}
	}

	return box;
};

/**
 * @param {character[][]} box
 * @return {character[][]}
 */
let rotateTheBox = (box) => {
	let rotated = [];
	box = box.reverse();

	for (let col = 0; col < box[0].length; col++) {
		for (let row = 0; row < box.length; row++) {
			if (rotated.length <= col) {
				rotated.push([[]]);
			}
			rotated[col][row] = box[row][col];
		}
	}

	return gravity(rotated);
};

let tests = {
	1: {
		input: [["#", ".", "#"]],
		expectedOutput: [["."], ["#"], ["#"]],
	},
	2: {
		input: [
			["#", ".", "*", "."],
			["#", "#", "*", "."],
		],
		expectedOutput: [
			["#", "."],
			["#", "#"],
			["*", "*"],
			[".", "."],
		],
	},
	3: {
		input: [
			["#", "#", "*", ".", "*", "."],
			["#", "#", "#", "*", ".", "."],
			["#", "#", "#", ".", "#", "."],
		],
		expectedOutput: [
			[".", "#", "#"],
			[".", "#", "#"],
			["#", "#", "*"],
			["#", "*", "."],
			["#", ".", "*"],
			["#", ".", "."],
		],
	},
};

let prettyPrint = (box) => {
    let pretty = "";

    for (let row = 0; row < box.length; row++) {
        for (let col = 0; col < box[row].length; col++) {
            pretty += box[row][col];
        }

        pretty += "\n";
    }

    return pretty;
};

Object.keys(tests).forEach((key) => {
	let test = tests[key];
	let output = rotateTheBox(test.input);
	let pass = JSON.stringify(output) === JSON.stringify(test.expectedOutput);
    let indicator = pass ? "PASS" : "FAIL";
    let message = pass ? "" : `\n${prettyPrint(output)}\n\n${prettyPrint(test.expectedOutput)}`;
	console.log(`Test ${key}: ${indicator}${message}`);
});

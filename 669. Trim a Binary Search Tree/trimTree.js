// Definition for a binary tree node.
function TreeNode(val, left, right) {
	this.val = val === undefined ? 0 : val;
	this.left = left === undefined ? null : left;
	this.right = right === undefined ? null : right;
}

/**
 * 
 * edge cases
 * * the tree is empty
 * * the root value is less than low
 * * the root value is higher than high
 */
let findNextValidNode = (node, low, high) => {
    if (!node) return null;
    if (node.val >= low && node.val <= high) return node;

    let side = (node.val > high) ? "left" : "right";

    return findNextValidNode(node[side], low, high);
};

let prune = (root, low, high) => {
    if (!root) return null;

    root.left = findNextValidNode(root.left, low, high);
    root.right = findNextValidNode(root.right, low, high);
    
    prune(root.left, low, high);
    prune(root.right, low, high);

    return root;
};

/**
 * @param {TreeNode} root
 * @param {number} low
 * @param {number} high
 * @return {TreeNode}
 */
let trimBST = (root, low, high) => {
    root = findNextValidNode(root, low, high);

    return prune(root, low, high);
};

let getRootVal = (values, low, high) => {
	let candidateRoots = values.filter((v) => v.val >= low && v.val <= high);
	return candidateRoots.length ? candidateRoots[0].val : null;
};

let bootstrap = (tree, low, high) => {
	// let rootVal = getRootVal(tree, low, high);
	return trimBST(tree, low, high);
};

let tests = [
	[new TreeNode(1, new TreeNode(0), new TreeNode(2)), 1, 2],
	[new TreeNode(3), 2, 2],
	[new TreeNode(3, new TreeNode(0, new TreeNode(1), new TreeNode(4)), new TreeNode(2)), 2, 3],
	[new TreeNode(2, new TreeNode(1), new TreeNode(3)), 1, 4],
	[new TreeNode(3, new TreeNode(0, null, new TreeNode(2, new TreeNode((1)))), new TreeNode(4)), 1, 3],
	[new TreeNode(3, new TreeNode(1, null, new TreeNode(2)), new TreeNode(4)), 1, 2],
	[new TreeNode(4, new TreeNode(2, new TreeNode(1), new TreeNode(3)), new TreeNode(5)), 1, 4],
	[new TreeNode(1, null, new TreeNode(2)), 2, 4],
	[new TreeNode(3, new TreeNode(1, null, new TreeNode(2)), new TreeNode(4)), 3, 4],
	[new TreeNode(3, new TreeNode(2, new TreeNode(1)), new TreeNode(4)), 2, 4],
	[new TreeNode(2, new TreeNode(0, null, new TreeNode(1)), new TreeNode(33, new TreeNode(25, new TreeNode(11, new TreeNode(10, new TreeNode(4, new TreeNode(3), new TreeNode(9, new TreeNode(5, null, new TreeNode(8, new TreeNode(7, new TreeNode(6))))))), new TreeNode(18, new TreeNode(12, null, new TreeNode(14, new TreeNode(13), new TreeNode(15, null, new TreeNode(17, new TreeNode(16))))), new TreeNode(24, new TreeNode(22, new TreeNode(21, new TreeNode(19, null, new TreeNode(20))), new TreeNode(23))))), new TreeNode(31, new TreeNode(29, new TreeNode(26, null, new TreeNode(27, null, new TreeNode(28))), new TreeNode(30)), new TreeNode(32))), new TreeNode(40, new TreeNode(34, null, new TreeNode(36, new TreeNode(35), new TreeNode(39, new TreeNode(38, new TreeNode(37))))), new TreeNode(45, new TreeNode(43, new TreeNode(42, new TreeNode(41)), new TreeNode(44)), new TreeNode(46, null, new TreeNode(48, new TreeNode(47), new TreeNode(49))))))), 25, 26],
];

tests.forEach((test) => console.log(bootstrap(...test)));
